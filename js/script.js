// function palindrom (x) {
//     let revX;
//     revX = x.split('').reverse().join('');
//     console.log(revX);
//     if (x == revX){
//         alert("Слово являється паліндромом");
//     }
//     else {
//         alert ("Слово не є паліндромом");
//     }
// }
// palindrom(prompt("Введіть слово"));
// function anag (x, y){
//     let anX;
//     let anY;
//     anX = x.split('').sort().join('');
//     anY = y.split('').sort().join('');
//     if (anX == anY){
//         alert("Слова являються анаграмами");
//     }
//     else{
//         alert("Слова не є анаграмами");
//     }
// }
// anag (prompt("Введіть перше слово"), prompt("Введіть друге слово"));
let game = [
    {
        question: "Столиця України?",
        answer: "а(Київ), б(Львів), в(Харків), г(Одеса)",
        corect: "а",
        win: 500,
        tot: 0
    },
    {
        question: "Столиця Польщі?",
        answer: "а(Катовіце), б(Львів), в(Варшава), г(Лодзь)",
        corect: "в",
        win: 1000,
        tot: 0
    },
    {
        question: "Дата хрещення Київської Русі?",
        answer: "а(999), б(888), в(988), г(899)",
        corect: "в",
        win: 2000,
        tot: 1000

    },
    {
        question: "День незалежності України ... серпня?",
        answer: "а(24), б(22), в(23), г(25)",
        corect: "а",
        win: 5000,
        tot: 1000

    },
    {
        question: "День незалежності США ... липня?",
        answer: "а(4), б(2), в(3), г(5)",
        corect: "а",
        win: 10000,
        tot: 1000

    },
    {
        question: "Площа прямокутника?",
        answer: "а(a+b), б(a-b), в(a/b), г(a*b)",
        corect: "г",
        win: 20000,
        tot: 10000

    },
    {
        question: "Площа квадрата?",
        answer: "а(a+а), б(a-а), в(a/а), г(a*а)",
        corect: "г",
        win: 50000,
        tot: 10000

    },
    {
        question: "Столиця Італії?",
        answer: "а(Мілан), б(Рим), в(Барі), г(Венеція)",
        corect: "б",
        win: 100000,
        tot: 10000

    },
    {
        question: "Кількість областей України?",
        answer: "а(22), б(33), в(21), г(25)",
        corect: "г",
        win: 200000,
        tot: 100000

    },
    {
        question: "Кількість штатів США?",
        answer: "а(50), б(53), в(51), г(49)",
        corect: "а",
        win: 500000,
        tot: 100000

    },
    {
        question: "Рік розвалу СССР?",
        answer: "а(1995), б(1989), в(1990), г(1991)",
        corect: "г",
        win: 1000000,
        tot: 100000

    }
];
for(const que of game){
    let x = prompt(que.question, que.answer);
    if (x == que.corect){
        alert (`Ви виграли ${que.win}`);
        let a = confirm("Бажаєте продовжити?");
        if (a == false){
            alert(`Ви завершили ігру та виграли ${que.win}`);
            break;
        }
    }
    else {
        alert(`Ви програли, правильна відповідь ${que.corect}, ваш виграш ${que.tot}`);
        break;
    }
}